let () =
  let argc = Array.length Sys.argv in
  if argc > 2
  then (
    print_endline "usage: plaidst [script]";
    exit 64)
  else if argc = 2
  then (
    let file_name = Sys.argv.(1) in
    print_string "running ";
    print_endline file_name;
    exit 0)
  else exit 0
;;
