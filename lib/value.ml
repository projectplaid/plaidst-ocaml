open Base

type t =
  | StBool of bool
  | StInt of int
  | StNumber of float
  | StString of string
  | StNil
[@@deriving show { with_path = false }, eq]
