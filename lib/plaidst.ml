module Scanner = Scanner
module Value = Value

let run source = Scanner.make_scanner source |> Scanner.scan_tokens
