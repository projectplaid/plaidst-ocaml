open Base

type token_type =
  | Nil
  | False
  | True
  | Self
  | Super
  | ThisContext
  | ByteArrayLiteral
  | Eof
[@@deriving eq, show { with_path = false }]

type token =
  { token_type : token_type
  ; lexeme : string
  ; literal : Value.t
  ; line : int
  }
[@@deriving show { with_path = false }]

type scanner =
  { source : string
  ; tokens : token list
  ; start : int
  ; current : int
  ; line : int
  }

let keywords =
  [ "nil", Nil
  ; "false", False
  ; "true", True
  ; "self", Self
  ; "super", Super
  ; "thisContext", ThisContext
  ]
;;

let make_scanner source = { source; tokens = []; start = 0; current = 0; line = 1 }
let is_at_end scanner = scanner.current >= String.length scanner.source
let advance_scanner scanner = { scanner with current = scanner.current + 1 }

let get_char scanner =
  if scanner.current >= String.length scanner.source
  then None
  else Some scanner.source.[scanner.current - 1]
;;

let get_lexeme scanner =
  String.sub scanner.source ~pos:scanner.start ~len:(scanner.current - scanner.start)
;;

let add_token scanner token_type =
  let token =
    { token_type
    ; lexeme = get_lexeme scanner
    ; literal = Value.StNil
    ; line = scanner.line
    }
  in
  { scanner with tokens = token :: scanner.tokens }
;;

let add_token_with_literal scanner token_type literal =
  let token = { token_type; lexeme = get_lexeme scanner; literal; line = scanner.line } in
  { scanner with tokens = token :: scanner.tokens }
;;

let peek scanner =
  if scanner.current >= String.length scanner.source
  then '\x00'
  else scanner.source.[scanner.current]
;;

let scan_token scanner =
  let scanner = advance_scanner scanner in
  match get_char scanner with
  | None -> scanner
  | Some c ->
    (match c with
     | _ ->
       StError.error scanner.line "Unexpected character";
       scanner)
;;

let rec scan_tokens scanner =
  if is_at_end scanner
  then (
    let token =
      { token_type = Eof; lexeme = ""; literal = Value.StNil; line = scanner.line }
    in
    List.rev (token :: scanner.tokens))
  else (
    let scanner = { scanner with start = scanner.current } in
    scan_tokens (scan_token scanner))
;;
