let test_simple () = Alcotest.(check string) "foo" "foo" "foo"

let () =
  let open Alcotest in
  run "Utils" [ "foo", [ test_case "Foo" `Quick test_simple ] ]
;;
